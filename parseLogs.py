import os
import re

regexTrainingTime = r'Epoch finished. Wall clock time is (\d*\.\d*) ms'
regexAccuracy = r', accuracy: (\d*\.\d*)'

def parseStats(file):
  accuracy = -1
  trainingTime = -1
  for line in file:
    matchesAcc = re.findall(regexAccuracy, line, re.S)
    matchesTT = re.findall(regexTrainingTime, line, re.S)
    
    if matchesAcc != []:
      accuracy = matchesAcc[-1]
    if matchesTT != []:
      trainingTime = matchesTT[-1]

  return trainingTime, accuracy

def collectLogFiles(directory):
  logfiles = []
  for logfile in os.listdir(directory):
    if logfile.endswith('.log'):
      logfiles.append(logfile)
  
  logfiles.sort()
  return logfiles

def main():
  logsDir = "logs/docker"
  logFiles = collectLogFiles(logsDir)
  for logFile in logFiles:
    with open(f"{logsDir}/{logFile}", 'r') as file:
      print(f"Log file: {logFile}")
      trainingTime, accuracy = parseStats(file)
      print(f"Training time: {trainingTime}")
      print(f"Accuracy: {accuracy}\n")

if __name__ == "__main__":
  main()