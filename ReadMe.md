# Scripts for running Spark stuff

## What is `runBigDL.sh`?

It's a shell script that submits one job / experiment on Spark. The system and hyper parameters for this job can be changed in the script.

To run this file, it is also required that `spark-submit` is on PATH.

## What is `runExperiments.py`?

It's a Python script that submits several jobs / experiments on the Spark master. The system and hyper parameters for these experiments must be defined in JSON files in the `experiments` folder. Output for an experiment JSON file `conf-1.json` is ported to `conf-1.json.x.log`, where `x` is the replication number. Any file that ends with `.json` is seen as a file that contains an experiment.

To run this file, it is also required that `spark-submit` is on PATH.

## What is `parseLogs.py`?

It's a Python script that parses the experiment logs in the `logs` folder for the final training times and accuracies.

## Docker Quick Start

Open a terminal and execute the following commands:
- `cd spark-docker`
- `docker-compose up`

Docker Compose will start building and launching the Spark Master and Spark Slave Docker containers. Once these have started, open another terminal and run the following command: 
- `python3 runExperiments.py`

All experiments defined in the `experiments` folder will now start running :)
