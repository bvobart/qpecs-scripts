import json
import os
import subprocess

master = "spark://127.0.1.1:7077"
dataset = "./datasets/mnist"
replications = 4

bigdlPythonAPI = "./bigdl-lib/bigdl-0.8.0-python-api.zip"
bigdlSparkJAR = "./bigdl-lib/bigdl-SPARK_2.3-0.8.0-jar-with-dependencies.jar"
bigdlSparkConf = "./bigdl-models/spark-bigdl.conf"

def coolPrint(text):
  print("---------------------------------------------------")
  print(f"--> {text}")
  print("---------------------------------------------------")

# Runs an experiment defined in a JSON config file in the ./experiments folder.
def runExperiment(configFile, logFile):
  with open(configFile) as cFile:
    config = json.loads(cFile.read())
    sysParams = config['systemParameters']
    hypParams = config['hyperParameters']
    modelFile = f"./bigdl-models/{hypParams['model']}.py"

    cmd  = f"spark-submit --master {master}"
    cmd += f" --driver-cores {sysParams['driverCores']}"
    cmd += f" --driver-memory {sysParams['driverMemory']}"
    cmd += f" --executor-cores {sysParams['executorCores']}"
    cmd += f" --executor-memory {sysParams['executorMemory']}"
    cmd += f" --total-executor-cores {sysParams['totalExecutorCores']}"
    cmd += f" --py-files {bigdlPythonAPI},{modelFile}"
    cmd += f" --properties-file {bigdlSparkConf}"
    cmd += f" --jars {bigdlSparkJAR}"
    cmd += f" --conf spark.driver.extraClassPath={bigdlSparkJAR}"
    cmd += f" --conf spark.executer.extraClassPath={bigdlSparkJAR}"
    cmd += f" --conf spark.worker.cleanup.enabled=true"
    cmd += f" {modelFile}"
    cmd += f" --action train"
    cmd += f" --dataPath {dataset}"
    cmd += f" --batchSize {hypParams['batchSize']}"
    cmd += f" --endTriggerNum {hypParams['maxEpoch']}"
    cmd += f" --learningRate {hypParams['learningRate']}"
    cmd += f" --learningrateDecay {hypParams['learningRateDecay']}"
    cmd += f" | tee {logFile}" # all console output is routed to log file

    # open process and print output to console
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in iter(p.stdout.readline, b''):
      print(line.decode('utf-8'))

    retCode = p.wait()
    if retCode != 0:
      coolPrint(f"Error: {configFile} exited with error code {retCode}")

def collectExperiments(directory):
  experiments = []
  for fileName in os.listdir(directory):
    fileName = f"{directory}/{fileName}"

    if fileName.endswith(".json"):
      experiments.append(fileName)
      
    elif not fileName.endswith(".log") or not fileName.endswith(".skip"):
      coolPrint(f"Skipping file that is not an experiment: {fileName}")
  
  return experiments

# Main function lists all files in ./experiments folder, then runs the experiments defined in the JSON files there.
def main():
  coolPrint("Starting...")
  experiments = collectExperiments("./experiments")
  experiments.sort()

  for replication in range(replications):
    for fileName in experiments:
      try:
        coolPrint(f"Running experiment: {fileName}")
        runExperiment(fileName, f"{fileName}.{replication}.log")
        coolPrint(f"Experiment completed!")
      except KeyError as e:
        coolPrint(f"Error: {fileName} is malformed: missing entry {e}")

  coolPrint("Finished!")

if __name__ == "__main__":
  main()