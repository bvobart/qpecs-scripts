#!/bin/bash

# Model. Pick one, comment out the other.
model="lenet5"
#model="bi-rnn"

# BigDL learning parameters
max_epoch=20
batch_size=128
learning_rate=0.01
learning_rate_decay=0.0002

# Spark Parameters
driver_cores=1
driver_memory=1G
executor_cores=1
executor_memory=1G
total_executor_cores=2

if [ -z $SPARK_MASTER ]; then
  master="spark://127.0.1.1:7077"
else
  master="spark://${SPARK_MASTER}"
fi

modelPyFile=./bigdl-models/${model}.py

spark-submit \
  --master ${master} \
  --driver-cores ${driver_cores} \
  --driver-memory ${driver_memory} \
  --total-executor-cores ${total_executor_cores} \
  --executor-cores ${executor_cores} \
  --executor-memory ${executor_memory} \
  --py-files ./bigdl-lib/bigdl-0.8.0-python-api.zip,${modelPyFile} \
  --properties-file ./bigdl-models/spark-bigdl.conf \
  --jars ./bigdl-lib/bigdl-SPARK_2.3-0.8.0-jar-with-dependencies.jar \
  --conf spark.driver.extraClassPath=./bigdl-lib/bigdl-SPARK_2.3-0.8.0-jar-with-dependencies.jar \
  --conf spark.executer.extraClassPath=./bigdl-lib/bigdl-SPARK_2.3-0.8.0-jar-with-dependencies.jar \
  ${modelPyFile} \
  --action train \
  --dataPath ./datasets/mnist \
  --batchSize ${batch_size} \
  --endTriggerNum ${max_epoch} \
  --learningRate ${learning_rate} \
  --learningrateDecay ${learning_rate_decay}
